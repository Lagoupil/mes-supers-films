-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : lun. 23 mars 2020 à 15:38
-- Version du serveur :  5.7.24
-- Version de PHP : 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `tp_film_20200323`
--

-- --------------------------------------------------------

--
-- Structure de la table `films`
--

CREATE TABLE `films` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `realisateur_id` int(11) NOT NULL,
  `duree` int(11) DEFAULT NULL,
  `note` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `jaquette` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `films`
--

INSERT INTO `films` (`id`, `title`, `realisateur_id`, `duree`, `note`, `description`, `jaquette`) VALUES
(1, 'Le Seigneur des anneaux : Les Deux Tours', 1, 120, 4, 'Après être entrés en Emyn Muil, Frodon Sacquet et Sam Gamegie rencontrent la créature Gollum, qui essaye de leur voler l\'Anneau par la force. Vaincu, il promet aux Hobbits de les guider jusqu\'au Mordor. Après avoir traversé l\'Emyn Muil et les marais des Morts, ils arrivent à la Morannon, la « Porte Noire » de Mordor. Cependant, elle est trop bien protégée pour qu\'ils entrent par là et Gollum leur propose de leur montrer le chemin secret de Cirith Ungol. Pendant le voyage, ils rencontrent une troupe avancée du Gondor, dirigée par Faramir, fils de l\'Intendant Denethor II et frère de Boromir. Il les fait prisonniers et découvre qu\'ils portent l\'Anneau unique. Il décide alors de les mener devant son père, mais, en traversant la cité détruite d\'Osgiliath, les soldats du Gondor sont confrontés aux forces de Sauron menées par des Nazgûl. Se rendant compte du pouvoir maléfique de l\'Anneau sur Frodon, qui a presque été pris par un des Nazgûl, Faramir se résout à les libérer pour qu\'ils accomplissent leur mission. ', 'https://images.vinted.net/thumbs/f800/02c74_kYNvoDToG22on29vzVFv1hP4.jpeg?1559043768-1d68985db08795af4589a1bd59c20810f6835fa8'),
(2, 'Le Seigneur des anneaux : La Communauté de l\'anneau', 1, 130, 4, 'Sur la Terre du Milieu, dans la paisible région de la Comté, vit le Hobbit Frodon Sacquet. Comme tous les Hobbits, Frodon est un bon vivant, amoureux de la terre bien cultivée et de la bonne chère. Orphelin alors qu\'il n\'était qu\'un enfant, il s\'est installé à Cul-de-Sac chez son oncle Bilbon, connu de toute la Comté pour les aventures extraordinaires qu\'il a vécues étant jeune et les trésors qu\'il en a tirés. Le jour de ses 111 ans, Bilbon donne une fête grandiose à laquelle est convié le puissant magicien Gandalf le Gris. C\'est en ce jour particulier que Bilbon décide de se retirer chez les Elfes pour y finir sa vie. Il laisse en héritage à Frodon son trou de Hobbit ainsi qu\'un anneau, qu\'il a autrefois trouvé dans la caverne d\'une créature nommée Gollum dans les Monts Brumeux, et qui a le pouvoir de rendre invisible quiconque le porte à son doigt. ', 'https://www.cdiscount.com/pdt2/6/0/4/1/700x700/3384442039604/rw/dvd-le-seigneur-des-anneaux-2-les-deux-tours.jpg'),
(3, 'Qui veut la peau de Roger Rabbit ?', 2, 90, 5, 'L\'action se passe à Los Angeles, en 1947 ; dans un univers où les Toons (personnages de dessins animés) ne sont pas de simples dessins mais des personnes réelles extravagantes. Ils habitent à Toonville, une zone adjacente à Hollywood, et se déplacent régulièrement chez les humains pour tourner des dessins animés. Roger Rabbit est un lapin acteur et héros de dessins animés des Maroon Cartoons. R. K. Maroon, propriétaire des studios Maroon Cartoons, demande au détective privé Eddie Valiant d\'enquêter sur une liaison possible qu\'entretiendraient Jessica Rabbit, la femme de Roger Rabbit et Marvin Acme, inventeur délirant et directeur de l\'ACME. Dans le passé, Valiant et son frère Teddy formaient une équipe de détectives réputée spécialisée dans les affaires de Toons. Mais depuis l\'assassinat de son frère par un Toon, Valiant s\'est retiré et a sombré dans l\'alcool. D\'abord réticent pour s\'occuper d\'une affaire avec un Toon, Valiant finit par accepter face à l\'insistance de Maroon. ', 'https://static.fnac-static.com/multimedia/FR/images_produits/FR/Fnac.com/ZoomPE/6/0/4/3459370409406/tsp20111027003222/Qui-veut-la-peau-de-Roger-Rabbit-Edition-Simple.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `medias`
--

CREATE TABLE `medias` (
  `id` int(11) NOT NULL,
  `film_id` int(11) NOT NULL,
  `media_type_id` int(11) NOT NULL,
  `duree` int(11) NOT NULL,
  `jaquette` varchar(255) NOT NULL,
  `est_collector` tinyint(1) NOT NULL,
  `description_feature` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `medias`
--

INSERT INTO `medias` (`id`, `film_id`, `media_type_id`, `duree`, `jaquette`, `est_collector`, `description_feature`) VALUES
(1, 2, 1, 120, 'https://www.cdiscount.com/pdt2/6/0/4/1/700x700/3384442039604/rw/dvd-le-seigneur-des-anneaux-2-les-deux-tours.jpg', 0, '[\"Commentaire Audio réalisateur\",\"Commentaire Audio acteurs\"]'),
(2, 2, 1, 150, 'https://images.fr.shopping.rakuten.com/photo/Le-Seigneur-Des-Anneaux-Les-Deux-Tours-Version-Cinema-Et-Version-Lo-DVD-Zone-2-876819910_L.jpg', 1, '[\"commentaire Real\", \"Commentaire acteurs\", \"Documentaire\" ]'),
(3, 1, 1, 120, 'https://images.vinted.net/thumbs/f800/02c74_kYNvoDToG22on29vzVFv1hP4.jpeg?1559043768-1d68985db08795af4589a1bd59c20810f6835fa8', 0, '[\"Commentaire Audio réalisateur\",\"Commentaire Audio acteurs\"]'),
(4, 3, 1, 90, 'https://static.fnac-static.com/multimedia/FR/images_produits/FR/Fnac.com/ZoomPE/6/0/4/3459370409406/tsp20111027003222/Qui-veut-la-peau-de-Roger-Rabbit-Edition-Simple.jpg', 0, '[]');

-- --------------------------------------------------------

--
-- Structure de la table `media_type`
--

CREATE TABLE `media_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `media_type`
--

INSERT INTO `media_type` (`id`, `name`, `type`) VALUES
(1, 'DVD', 'optique'),
(2, 'Bluray', 'optique');

-- --------------------------------------------------------

--
-- Structure de la table `realisateurs`
--

CREATE TABLE `realisateurs` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `realisateurs`
--

INSERT INTO `realisateurs` (`id`, `name`) VALUES
(1, 'Peter Jackson'),
(2, 'Robert Zemeckis');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `media_type`
--
ALTER TABLE `media_type`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `realisateurs`
--
ALTER TABLE `realisateurs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `films`
--
ALTER TABLE `films`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `medias`
--
ALTER TABLE `medias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `media_type`
--
ALTER TABLE `media_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `realisateurs`
--
ALTER TABLE `realisateurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
